## Netzon Tensorflow Docker Image

### Build

``` bash
docker build -f Dockerfile -t netzon/netzon-docker-tensorflow:3.8-slim-buster-tensorflow-2.4.1 .
docker push netzon/netzon-docker-tensorflow:3.8-slim-buster-tensorflow-2.4.1
```