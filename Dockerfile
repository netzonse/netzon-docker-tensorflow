FROM python:3.8-slim-buster

RUN apt-get update && apt-get -y upgrade && apt-get install -y libpq-dev gcc g++ \
  && python -m pip install --upgrade pip \
  && python -m pip install \
        autopep8 \
        flake8 \
        mypy \
        mypy-extensions \
        numpy \
        pyflakes \
        pytest \
        pytest-timeout \
        pytest-mock \
        tensorflow \
        shap